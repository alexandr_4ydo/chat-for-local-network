﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace MyTestServer
{
    class Server_BL
    {
        IPHostEntry IpHost;
        IPAddress Adress;
        IPEndPoint EndPoint;
        Socket SListener;
        String UserName;

        public Server_BL()
        {
            IpHost = Dns.GetHostEntry("localhost");
            Adress = IpHost.AddressList[0];
            EndPoint = new IPEndPoint(Adress, 11000);
            SListener = new Socket(Adress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        }

        public void StartListening()
        {
            string Data = null;
            byte[] Bytes = new byte[1024]; 
            try
            {
                SListener.Bind(EndPoint);
                SListener.Listen(10);
                while(true)
                {
                    Socket handler = SListener.Accept();
                    int BytesRec = handler.Receive(Bytes);
                    Data += Encoding.UTF8.GetString(Bytes);//Надо перепроверить с перегрузкой\
                    string txtMessage = "Сообщение";
                    byte[] message = Encoding.UTF8.GetBytes(txtMessage);
                    handler.Send(message);
                    Console.WriteLine(Data);

                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

    }
}
