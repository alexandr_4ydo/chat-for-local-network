﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace ServerClient_MyLibrary
{
    [Serializable]
     public class User: SuperPacket
    {
        string name;
        IPAddress address;

        public string Name
        {
            get { return name; }
        }

        public IPAddress Address
        {
            get { return address; }
        }
        public User() : base(TypeOfObjett.Empty) { }
        public User(string Name, IPAddress Address): base(TypeOfObjett.User)
        {
            this.name = Name;
            this.address = Address;
        }
    }
}
