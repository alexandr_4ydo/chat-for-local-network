﻿namespace LocalChat
{
    partial class Chat
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.OneMessageTB = new System.Windows.Forms.RichTextBox();
            this.AllMessagessTB = new System.Windows.Forms.RichTextBox();
            this.Send_btn = new System.Windows.Forms.Button();
            this.UsersListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Name_label = new System.Windows.Forms.Label();
            this.UserName_TxtBox = new System.Windows.Forms.TextBox();
            this.AddButton = new System.Windows.Forms.Button();
            this.Recipient_label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // OneMessageTB
            // 
            this.OneMessageTB.Enabled = false;
            this.OneMessageTB.Location = new System.Drawing.Point(208, 444);
            this.OneMessageTB.Name = "OneMessageTB";
            this.OneMessageTB.Size = new System.Drawing.Size(562, 106);
            this.OneMessageTB.TabIndex = 1;
            this.OneMessageTB.Text = "";
            // 
            // AllMessagessTB
            // 
            this.AllMessagessTB.Enabled = false;
            this.AllMessagessTB.Location = new System.Drawing.Point(208, 1);
            this.AllMessagessTB.Name = "AllMessagessTB";
            this.AllMessagessTB.Size = new System.Drawing.Size(562, 408);
            this.AllMessagessTB.TabIndex = 2;
            this.AllMessagessTB.Text = "";
            // 
            // Send_btn
            // 
            this.Send_btn.Enabled = false;
            this.Send_btn.Location = new System.Drawing.Point(686, 415);
            this.Send_btn.Name = "Send_btn";
            this.Send_btn.Size = new System.Drawing.Size(75, 23);
            this.Send_btn.TabIndex = 3;
            this.Send_btn.Text = "Send";
            this.Send_btn.UseVisualStyleBackColor = true;
            this.Send_btn.Click += new System.EventHandler(this.Send_btn_Click);
            // 
            // UsersListBox
            // 
            this.UsersListBox.BackColor = System.Drawing.SystemColors.Menu;
            this.UsersListBox.Enabled = false;
            this.UsersListBox.FormattingEnabled = true;
            this.UsersListBox.Location = new System.Drawing.Point(2, 65);
            this.UsersListBox.Name = "UsersListBox";
            this.UsersListBox.Size = new System.Drawing.Size(200, 485);
            this.UsersListBox.TabIndex = 4;
            this.UsersListBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.UsersListBox_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Users";
            // 
            // Name_label
            // 
            this.Name_label.AutoSize = true;
            this.Name_label.Location = new System.Drawing.Point(13, 13);
            this.Name_label.Name = "Name_label";
            this.Name_label.Size = new System.Drawing.Size(35, 13);
            this.Name_label.TabIndex = 6;
            this.Name_label.Text = "Name";
            // 
            // UserName_TxtBox
            // 
            this.UserName_TxtBox.Location = new System.Drawing.Point(55, 13);
            this.UserName_TxtBox.Name = "UserName_TxtBox";
            this.UserName_TxtBox.Size = new System.Drawing.Size(100, 20);
            this.UserName_TxtBox.TabIndex = 7;
            this.UserName_TxtBox.Text = "User1";
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(162, 13);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(40, 23);
            this.AddButton.TabIndex = 8;
            this.AddButton.Text = "Add";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // Recipient_label
            // 
            this.Recipient_label.AutoSize = true;
            this.Recipient_label.Location = new System.Drawing.Point(208, 420);
            this.Recipient_label.Name = "Recipient_label";
            this.Recipient_label.Size = new System.Drawing.Size(58, 13);
            this.Recipient_label.TabIndex = 9;
            this.Recipient_label.Text = "Recipient :";
            // 
            // Chat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 552);
            this.Controls.Add(this.Recipient_label);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.UserName_TxtBox);
            this.Controls.Add(this.Name_label);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.UsersListBox);
            this.Controls.Add(this.Send_btn);
            this.Controls.Add(this.AllMessagessTB);
            this.Controls.Add(this.OneMessageTB);
            this.Name = "Chat";
            this.Text = "LocalChat";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Chat_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox OneMessageTB;
        private System.Windows.Forms.RichTextBox AllMessagessTB;
        private System.Windows.Forms.Button Send_btn;
        private System.Windows.Forms.ListBox UsersListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Name_label;
        private System.Windows.Forms.TextBox UserName_TxtBox;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Label Recipient_label;
    }
}

