﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using ServerClient_MyLibrary;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace MyTestServer
{
    class Server_BL
    {
        IPHostEntry m_IPhost;
        IPAddress m_address;
        IPEndPoint m_endPoint;
        Socket m_sListener, m_handler;
        BinaryFormatter m_packetSerialiser;
        User m_user;
        List<string> m_users;
        Dictionary<string, Socket> m_usersDictionary;


        public Server_BL()
        {
            m_packetSerialiser = new BinaryFormatter();
            m_IPhost = Dns.GetHostEntry("localhost");
            m_address = IPAddress.Any;
            m_endPoint = new IPEndPoint(m_address, 11000);
            m_sListener = new Socket(m_address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            m_users = new List<string>();
            m_usersDictionary = new Dictionary<string, Socket>();

        }

        public void Listening()
        {
            m_sListener.Bind(m_endPoint);
            m_sListener.Listen(10);
            while (true)
            {
                m_handler = m_sListener.Accept();
                Thread NewSocketThread = new Thread(new ParameterizedThreadStart(Receiving));
                NewSocketThread.Start(m_handler);

            }
        }

        bool SocketConnected(Socket s)
        {
            bool part1 = s.Poll(1000, SelectMode.SelectRead);
            bool part2 = (s.Available == 0);
            if (part1 && part2)
                return false;
            else
                return true;
        }

        public void Receiving(object o)
        {
            Socket Handler = (Socket)o;
            byte[] userInBytes;
            while (true)
            {
                if (Handler != null)
                {
                    userInBytes = new byte[Handler.ReceiveBufferSize];

                    try
                    {
                        int lenght = Handler.Receive(userInBytes);

                        using (MemoryStream mystream = new MemoryStream(userInBytes, 0, lenght))
                        {
                            SuperPacket superPacket = (SuperPacket)m_packetSerialiser.Deserialize(mystream);
                            switch (superPacket.objectType)
                            {
                                case SuperPacket.TypeOfObjett.Message:
                                    {
                                        GetSendMessage(superPacket);
                                        break;
                                    }
                                case SuperPacket.TypeOfObjett.User:
                                    {
                                        GetSendUser(superPacket, Handler);
                                        break;
                                    }
                                case SuperPacket.TypeOfObjett.Empty:
                                    {
                                        break;
                                    }
                            }
                        }
                    }
                    catch (SocketException)
                    {
                        m_usersDictionary.Remove(GetUserBySocket(Handler));
                        SendUsersList();
                        return;
                    }
                }
            }
        }

        void GetSendMessage(SuperPacket UserPacket)
        {
            UsersMessage message = (UsersMessage)UserPacket;
            using (MemoryStream MessageStream = new MemoryStream())
            {
                m_packetSerialiser.Serialize(MessageStream, message);
                m_usersDictionary[message.Recipient].Send(MessageStream.GetBuffer());
            }          
        }

        void GetSendUser(SuperPacket UserPacket, Socket Handler)
        {
            m_user = (User)UserPacket;
            if (!m_usersDictionary.ContainsKey(m_user.Name))
            {
                m_usersDictionary.Add(m_user.Name, Handler);
                SendUsersList();
            }
            else
            {

                User Empty = new User();
                using (MemoryStream MessageStream = new MemoryStream())
                {
                    m_packetSerialiser.Serialize(MessageStream, Empty);
                    Handler.Send(MessageStream.GetBuffer());
                }
            }
        }

        string GetUserBySocket(Socket UserSocket)
        {
            string UserName = "";
            foreach (string u in m_usersDictionary.Keys)
            {
                if (m_usersDictionary[u] == UserSocket)
                {
                    UserName = u;
                    break;
                }
            }
            return UserName;

        }
        void SendUsersList()
        {
            using (MemoryStream Sendstream = new MemoryStream())
            {
                m_users = new List<string>(m_usersDictionary.Keys);
                ListOfUsers UsList = new ListOfUsers(m_users);
                m_packetSerialiser.Serialize(Sendstream, UsList);
                foreach (string u in m_users)
                {
                    try
                    {
                        m_usersDictionary[u].Send(Sendstream.GetBuffer());
                    }
                    catch
                    {
                        //users.Remove(u.Name);
                        m_usersDictionary.Remove(u);
                        SendUsersList();
                        return;  
                    }
                }
            }
        }

    }
}
