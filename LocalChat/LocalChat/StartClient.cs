﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Net.Sockets;

namespace LocalChat
{
    internal static class StartClient
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        public static Thread ListenThread;
        [STAThread]
        static void Main()
        {
                IUserActions useractions = new Controller();

                ListenThread = new Thread(new ThreadStart(useractions.ListenServer));
                ListenThread.Start();

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Chat(useractions));
                ListenThread.Abort();
        }
    }
}
