﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerClient_MyLibrary
{
    [Serializable]
    public class UsersMessage : SuperPacket
    {
        string sender;

        string recipient;
        string messageText;

        public string Sender
        {
            get { return sender; }
        }
        public string MessageText
        {
            get { return messageText; }
        }
        public string Recipient
        {
            get { return recipient; }
        }
        public UsersMessage(string Recipient, string Message, string Sender)
            : base(TypeOfObjett.Message)
        {
            this.sender = Sender;
            this.recipient = Recipient;
            this.messageText = Message;
        }
    }
}
