﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerClient_MyLibrary
{
    [Serializable]
     public abstract class SuperPacket
    {
        public enum TypeOfObjett 
        {   
            User, 
            Message, 
            ListOfUserNames,
            Empty
        };
        public TypeOfObjett objectType;

        public SuperPacket(TypeOfObjett Type)
        {
            objectType = new TypeOfObjett();
            this.objectType = Type;
        }
    }
}
