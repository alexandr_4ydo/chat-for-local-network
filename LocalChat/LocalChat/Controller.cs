﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using ServerClient_MyLibrary;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace LocalChat
{
    public class Controller : IUserActions
    {
        IPHostEntry m_IPhost;
        IPAddress m_address;
        Socket m_clientSocket;
        BinaryFormatter m_serializer;
        IUserCallBacks m_userCallBacks;
        List<string> m_userNames;

        public Controller()
        {
            try
            {
                m_userNames = new List<string>();
                m_clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
                m_IPhost = Dns.GetHostEntry("Bogdanov-PC");

                m_address = m_IPhost.AddressList[1];
                m_clientSocket.Connect(m_address, 11000);
                m_serializer = new BinaryFormatter();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public void OnAddUser(string UserName)
        {
            User user = new User(UserName, m_address);
            using (MemoryStream mystream = new MemoryStream())
            {
                m_serializer.Serialize(mystream, user);
                m_clientSocket.Send(mystream.GetBuffer());
            }
        }

        public void SetCallBack(IUserCallBacks UserCallBack)
        {
            m_userCallBacks = UserCallBack;
        }

        public void ListenServer()
        {           
                byte[] usersInBytes;
                while (true)
                {
                    usersInBytes = new byte[m_clientSocket.ReceiveBufferSize];
                    using (MemoryStream stream = new MemoryStream(usersInBytes))
                    {
                        m_clientSocket.Receive(usersInBytes);
                        SuperPacket SP = (SuperPacket)m_serializer.Deserialize(stream);
                        switch (SP.objectType)
                        {
                            case SuperPacket.TypeOfObjett.ListOfUserNames:
                                {
                                    ListOfUsers listOfUsers = (ListOfUsers)SP;
                                    m_userNames = listOfUsers.Userslist;
                                    m_userCallBacks.OnUserList(listOfUsers.Userslist);
                                    break;
                                }
                            case SuperPacket.TypeOfObjett.Message:
                                {
                                    UsersMessage message = (UsersMessage)SP;
                                    m_userCallBacks.GetMessage(message.Recipient, message.MessageText, message.Sender);
                                    break;
                                }
                            case SuperPacket.TypeOfObjett.Empty:
                                {
                                    m_userCallBacks.ShowThatNameUsed();
                                    break;
                                }
                        }
                    }

                }          
        }

        public void SendMessage(string Recipient, string Message, string Sender)
        {
            UsersMessage message = new UsersMessage(Recipient, Message, Sender);
            using (MemoryStream messagestream = new MemoryStream())
            {
                m_serializer.Serialize(messagestream, message);
                m_clientSocket.Send(messagestream.GetBuffer());
            }
        }
    }

    public interface IUserActions
    {
        void OnAddUser(string UserName);
        void SetCallBack(IUserCallBacks UserCallBack);
        void SendMessage(string Recipient, string Message, string Sender);
        void ListenServer();
    }

    public interface IUserCallBacks
    {
        void OnUserList(List<string> UserNames);
        void GetMessage(string Recipient, string Message, string Sender);
        void ShowThatNameUsed();
    }
}
