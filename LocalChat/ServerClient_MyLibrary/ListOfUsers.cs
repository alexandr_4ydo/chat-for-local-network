﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerClient_MyLibrary
{
    [Serializable]
    public class ListOfUsers: SuperPacket
    {
        List<string> userslist;
        public List<string> Userslist
        {
            get { return userslist; }
        }
        public ListOfUsers(List<string> UsersList)
            : base(TypeOfObjett.ListOfUserNames)
        {
            this.userslist = new List<string>();
            this.userslist = UsersList;
        }
    }
}
