﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//Клиент:Нужно постоянно принимать с сервера список юзеров и постоянно его обновлять
//Сервер:Нужно что бы не падал сервер, при отпадании одного из клиентов


namespace LocalChat
{
    public partial class Chat : Form, IUserCallBacks
    {
        
        List<string> users;
        IUserActions UserActions;
        delegate void SetUsersListDelegate(List<string> users);
        delegate void GetMessageDelegate(string Recipient, string Message, string Sender);
        GetMessageDelegate GetMessageDel;
        SetUsersListDelegate SetUsersListD;
        string usName;
        public Chat(IUserActions UserActions)
        {
            this.UserActions = UserActions;
            InitializeComponent();
            users = new List<string>();
            UserActions.SetCallBack(this);
            SetUsersListD = new SetUsersListDelegate(OnUList);
            GetMessageDel = new GetMessageDelegate(GetMess);
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
                UserActions.OnAddUser(UserName_TxtBox.Text);     
        }

        public void OnUserList(List<string> UserNames)
        {
            this.Invoke(SetUsersListD, UserNames);
        }
        public void OnUList(List<string> UserNames)
        {
            UsersListBox.Items.Clear();
     
                foreach (string u in UserNames)
                {
                    UsersListBox.Items.Add(u);
                    AddButton.Enabled = false;
                    UserName_TxtBox.Enabled = false;
                    UsersListBox.Enabled = true;
                    AllMessagessTB.Enabled = true;
                    OneMessageTB.Enabled = true;
                    Send_btn.Enabled = true;
                }
            
        }
        private void UsersListBox_MouseClick(object sender, MouseEventArgs e)
        {
            if(UsersListBox.SelectedItem != null)
            {
                usName = (string)UsersListBox.SelectedItem;
                Recipient_label.Text = "Recipient : " + UsersListBox.SelectedItem;
            }
        }

        private void Send_btn_Click(object sender, EventArgs e)
        {
            if (usName != null )
            {
                if(OneMessageTB.Text != null)
                {
                    UserActions.SendMessage(usName, OneMessageTB.Text, UserName_TxtBox.Text);
                    GetMess((string)usName, OneMessageTB.Text, UserName_TxtBox.Text);
                    OneMessageTB.Clear();
                }
            }
            else
            {
                MessageBox.Show("Select the recipient!");
            }
        }
        public void GetMessage(string Recipient, string Message, string Sender)
        {
            Invoke(GetMessageDel,Recipient,Message,Sender);//
        }
        public void GetMess(string Recipient, string Message, string Sender)
        {
            AllMessagessTB.Text += String.Format("{0}, To {1} \n {2} \n", Sender, Recipient, Message);
        }
        public void ShowThatNameUsed()
        {
            MessageBox.Show("This name already used!");
        }

        private void Chat_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

    }
}
